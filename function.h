#ifndef function_h
#define function_h

//////////////////////////////////////////////////////////////////////////////////////////

// 複素数の構造体
typedef struct {
    double real;
    double image;
} complex;

//////////////////////////////////////////////////////////////////////////////////////////

// 複素数の関数
complex makeComp(double real, double image);
complex addComp(complex a, complex b);
complex cmulComp(complex a, double k);
double getR2(complex a);
complex invComp(complex a);
double getR(complex a);
void printComp(complex a);
complex makeCompRT(double r, double theta);
complex subComp(complex a, complex b);
complex mulComp(complex a, complex b);
complex conjComp(complex a);
complex divComp(complex a, complex b);
double getTheta(complex a);
void printCompRT(complex a);

//////////////////////////////////////////////////////////////////////////////////////////

// 行列計算の関数
void delta(complex *eqA, complex *eqB, int column, int dim);
complex cofactorExpansion(complex *eq, int row, int column, int dim);
complex determinant(complex *eq, int row, int dim);
void cramer(complex *ans, complex *eqA, complex *eqB, int row, int dim);

//////////////////////////////////////////////////////////////////////////////////////////

#endif