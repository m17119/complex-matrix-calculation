#include <math.h>
#include <stdio.h>

#include "function.h"

int main() {
    int i, j, dim;
    int count = 0;
    complex eqA[256] = {};
    complex eqB[256] = {};
    complex ans[256] = {};

    printf("n次の正方行列\n");
    printf("nの値を入力してください\n");
    scanf("%d", &dim);

    for (i = 0; i < dim; i++) {
        for (j = 0; j < dim; j++) {
            // 行列aの代入
            printf("行列入力：a[%d][%d]\n", i, j);
            printf("実数部\n");
            scanf("%lf", &eqA[count].real);
            printf("虚数部\n");
            // eqA[count].image = 0.0;
            scanf("%lf", &eqA[count].image);
            count++;
        }
        // 行列bの代入
        printf("行列入力：b[%d]\n", i);
        printf("実数部\n");
        scanf("%lf", &eqB[i].real);
        printf("虚数部\n");
        // eqB[count].image = 0.0;
        scanf("%lf", &eqB[i].image);
    }

    cramer(ans, eqA, eqB, 0, dim);

    // 解の確認
    for (i = 0; i < dim; i++) {
        printf("ans[%d] = ", i);
        printComp(ans[i]);
    }

    return 0;
}