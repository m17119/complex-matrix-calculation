TARGET=main
FUNC=function.o
HEADERS=function.h
MAIN=$(TARGET).o
LIBS=-lm
CFLAGS=-Wall -g
CC=gcc
CHCP = chcp 65001
EXE=.exe
RM := cmd.exe /C del


exec: $(TARGET)
	$(CHCP)
	./$?

$(TARGET): $(MAIN) $(FUNC) $(HEADERS)
	$(CC) -o $@ $(CFLAGS) $(MAIN) $(FUNC) $(LIBS)

clean:
	$(RM) $(TARGET)$(EXE) $(MAIN) $(FUNC)