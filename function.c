#include "function.h"

#include <math.h>
#include <stdio.h>

//////////////////////////////////////////////////////////////////////////////////////////

// 複素数の関数
complex makeComp(double real, double image) {
    complex ans;
    ans.real = real;
    ans.image = image;
    return ans;
}

complex addComp(complex a, complex b) {
    return makeComp(a.real + b.real, a.image + b.image);
}

complex cmulComp(complex a, double k) {
    return makeComp(k * a.real, k * a.image);
}

double getR2(complex a) { return a.real * a.real + a.image * a.image; }

complex invComp(complex a) { return cmulComp(conjComp(a), 1 / getR2(a)); }

void printComp(complex a) { printf("%f%+fj\n", a.real, a.image); }

complex mulComp(complex a, complex b) {
    return makeComp(a.real * b.real - a.image * b.image,
                    a.real * b.image + a.image * b.real);
}

complex conjComp(complex a) { return makeComp(a.real, -a.image); }

complex divComp(complex a, complex b) { return mulComp(invComp(b), a); }

//////////////////////////////////////////////////////////////////////////////////////////

// 行列計算の関数
void delta(complex *eqA, complex *eqB, int column, int dim) {
    int i;

    for (i = 0; i < dim * dim; i++) {
        if (i % dim == column) {
            *eqA = *eqB;
            eqB++;
        }
        eqA++;
    }
}

complex cofactorExpansion(complex *eq, int row, int column, int dim) {
    int i;
    int count = 0;
    complex ans;

    ans = *eq;
    if (dim != 1) {
        ans = mulComp(makeComp(pow(-1.0, row + column), 0.0),
                      eq[dim * row + column]);
        for (i = 0; i < dim * dim; i++) {
            if (i / dim != row && i % dim != column) {
                eq[count] = eq[i];
                count++;
            }
        }
        for (i = count; i < dim * dim; i++) {
            eq[i] = makeComp(0.0, 0.0);
        }
    }
    return ans;
}

complex determinant(complex *eq, int row, int dim) {
    int i, j;
    int count = dim;
    complex ans;
    complex finalAns = makeComp(0.0, 0.0);
    complex parameter[256];

    for (i = 0; i < dim * dim; i++) {
        parameter[i] = eq[i];
    }

    for (i = 0; i < dim; i++) {
        ans = cofactorExpansion(parameter, row, i, dim);
        count--;
        if (count == 1) {
            ans = mulComp(ans, cofactorExpansion(parameter, row, i, count));
        } else {
            ans = mulComp(ans, determinant(parameter, row, count));
        }
        count++;
        for (j = 0; j < dim * dim; j++) {
            parameter[j] = eq[j];
        }
        finalAns = addComp(finalAns, ans);
    }
    return finalAns;
}

void cramer(complex *ans, complex *eqA, complex *eqB, int row, int dim) {
    int i, j;
    complex parameter[256];

    for (i = 0; i < dim * dim; i++) {
        parameter[i] = eqA[i];
    }

    for (i = 0; i < dim; i++) {
        delta(parameter, eqB, i, dim);
        ans[i] = divComp(determinant(parameter, row, dim),
                         determinant(eqA, row, dim));
        for (j = 0; j < dim * dim; j++) {
            parameter[j] = eqA[j];
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////